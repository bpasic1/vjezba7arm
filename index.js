const dateObject = new Date();
// trenutni datum i postavljena 0 radi formatiranja
const date = ( "0" +`${dateObject.getDate()}`).slice(-2);
 
// trenutni mjesec
const month = ("0" +`${dateObject.getMonth() + 1}`).slice(-2);
 
// trenutna godina
const year = dateObject.getFullYear();
 
const hours = dateObject.getHours();
const minutes = dateObject.getMinutes();
const seconds = dateObject.getSeconds();

//Ispis imena studenta i index
console.log("Student: Benjamin Pasic\nIndex: 18619");
 
// Ispis datuma i vremena
console.log("Danasnji datum i vrijeme je:", `${date}-${month}-${year} ${hours}:${minutes
    .toString()
    .padStart(2, '0')}:${seconds
        .toString()
        .padStart(2, '0')}`);